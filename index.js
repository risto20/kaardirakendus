
window.onload= function() {
  document.querySelector('.set-today').value=(new Date()).toISOString().substr(0,10);
}

//Kasutan Arcgis map maailma kaarti oma rakenduses

require([
        "esri/Map",
        "esri/views/MapView",
        "esri/Graphic",
        "esri/layers/GraphicsLayer"
    ],

    function(Map, MapView, Graphic, GraphicsLayer) {

        var map = new Map({
            basemap: "topo-vector"
        });

        var view = new MapView({
            container: "viewDiv",
            map: map,
            center: [-125.39722, 70.99257],
            zoom: 4
        });

        //*** Add div element to show coordates ***//
        var coordsWidget = document.createElement("div");
        coordsWidget.id = "coordsWidget";
        coordsWidget.className = "esri-widget esri-component";
        coordsWidget.style.padding = "7px 15px 5px";
        view.ui.add(coordsWidget, "bottom-right");

        let insertedDate;

        //*** Update lat, lon, zoom and scale ***//
        function showCoordinates(pt) {

            showLat = pt.latitude;
            showLong = pt.longitude;

            //Läbi selle kuvatakse HTMLis show id sees parameetrid showLat ja showLong
            document.getElementById('show').innerHTML = `Latitude: ${showLat} <br> Longitude: ${showLong}`;

            //Nupuvajutusel maailmakaardil käivitatakse selected funktsioon mille id on htmlis viewDiv
            document.getElementById("viewDiv").onclick = selected;

            let lat;
            let lon;


            function selected() {

                //saame kasutaja sisestatud kuupäeva htmlist kätte date väärtusena
                let date = document.getElementById('date').value;
                insertedDate = date;

                lat = showLat;
                lon = showLong;

        
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: 'coordinate/ajax.php',
                    data: { latitude: showLat, longitude: showLong, date: insertedDate },
                    success: function(data) {
                        $('#dayLength').html(data['day_length']),
                            $('#latitude').html(data['latitude']),
                            $('#longitude').html(data['longitude']),
                            $('#showDate').html(insertedDate),
                            $('#dayStart').html(data['dayStart']),
                            $('#dayEnd').html(data['dayEnd'])
                    }
                })

                var graphicsLayer = new GraphicsLayer();
                map.add(graphicsLayer);


                var simpleMarkerSymbol = {
                    type: "simple-marker",
                    color: [226, 119, 40], // orange
                    outline: {
                        color: [255, 255, 255], // white
                        width: 1
                    }
                };
                
                //Nupuvajutusele salvestub täpp maailmakaardil, et näha kuhu on vajutatud.
                var point = {
                    type: "point",
                    longitude: lon,
                    latitude: lat
                }

                var pointGraphic = new Graphic({
                    geometry: point,
                    symbol: simpleMarkerSymbol,
                });
                graphicsLayer.add(pointGraphic);
            };
        };

        //*** Add event and show center coordinates after the view is finished moving e.g. zoom, pan ***//
        view.watch(["stationary"], function() {
            showCoordinates(view.center);
        });

        //*** Add event to show mouse coordinates on click and move ***//
        view.on(["pointer-down", "pointer-move"], function(evt) {
            showCoordinates(view.toMap({ x: evt.x, y: evt.y }));
        });

      
  });
    
