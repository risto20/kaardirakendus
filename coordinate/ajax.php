<?php

//Siin väärtustame saadud väärtuse läbi ajaxi requesti
$latitude = $_POST['latitude'];
$longitude = $_POST['longitude'];
$insertedDate = $_POST['date'];

//Määrame antud olukorras ära timezone
date_default_timezone_set('Europe/Tallinn');

//Määran muutujale funktsiooni tulemid. Et kätte saada päeva andmed, kasutame date_sun_info funktsiooni, mille parameetrid on kuupäev, latitude ja longitude.
$sun_info = date_sun_info(strtotime($insertedDate), $latitude, $longitude);

if (isset($sun_info['sunrise'])) {
$dayStart = date("H:i:s", $sun_info['sunrise']);
};

if (isset($sun_info['sunset'])) {
  $dayEnd = date("H:i:s", $sun_info['sunset']);
  };


//Väärtustame muutuja $difference, et leida päeva pikkus päikesetõusu ja päikeseloojangu vahel
$difference = ($sun_info['sunset']) - ($sun_info['sunrise']);

//Päeva pikkus tunni, minuti ja sekundi täpsusega gmdate funktsiooni abil
$gmdate = gmdate("H:i:s", $difference);


//Et ajaxis kätte saada parameetrid, koostame array.
$arr = array (
  'day_length' => $gmdate,
  'longitude' => $longitude,
  'latitude' => $latitude,
  'date' => $insertedDate, 
  'dayStart' => $dayStart,
  'dayEnd' => $dayEnd
);

//Määrame $output muutujale json formaadis muutuja $arr, mille sees on meil array, kust hakkame infot kätte saama ajaxis
$output = json_encode($arr);

//kuvame andmed array seest ajaxi requesti
echo $output;

